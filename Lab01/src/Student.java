//package oop.lab1;
/**
 * A student information includes ID
 * @author Pipatpol Tanavongchinda
 * @version 2015.01.14 16:57 
 */
//TODO Write class Javadoc
public class Student extends Person {
	private long id;
	
	/**
	 * Initialize a new student.
	 * @param name is the name of student.
	 * @param id is the ID of student.
	 */
	//TODO Write constructor Javadoc
	public Student(String name, long id) {
		super(name); // name is managed by Person
		this.id = id;
	}

	/** return a string representation of this Student. */
	public String toString() {
		return String.format("Student %s (%d)", getName(), id);
	}

	//TODO Write equals
	public boolean equals( Object obj) {
		//return this.id == other.id;
		
		if(obj == null )
		{
			return false;
		}
		if(obj.getClass() != this.getClass())
		{
			return false;
		}
		Student other = (Student) obj;
		if( this.id == other.id)
		{
			return true;
		}
		return false;
	}
}
