package coinpurse; 

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
/**
 *  A Valuable purse contains money.
 *  You can insert money, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the Valuable purse decides which
 *  money to remove.
 *  
 *  @author Pipatpol.T
 */
public class Purse {
    /** Collection of money in the purse. */
    private List<Valuable> money;
    /** Capacity is maximum NUMBER of money the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    
    private static ValueComparator comparator = new ValueComparator();
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of money you can put in purse.
     */
    public Purse( int capacity ) {
    	this.capacity = capacity;
    	money = new ArrayList<Valuable>();
    }

    /**
     * Count and return the number of money in the purse.
     * This is the number of money, not their value.
     * @return the number of money in the purse
     */
    public int count() 
    { 
    	return money.size();	
    }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() 
    {
    	int total_value = 0;
    	for(int count = 0;count<money.size();count++)
    	{
    		total_value += (money.get(count)).getValue();
    	}
    	return total_value; 
    }

    
    /**
     * Return the capacity of the Valuable purse.
     * @return the capacity
     */
    public int getCapacity() 
    { 
    	return this.capacity; 
    }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
        return money.size() == this.capacity;
    }

    /** 
     * Insert a Valuable into the purse.
     * The Valuable is only inserted if the purse has space for it
     * and the Valuable has positive value.  No worthless money!
     * @param Valuable is a Valuable object to insert into purse
     * @return true if Valuable inserted, false if can't insert
     */
    public boolean insert( Valuable Valuable ) {
        // if the purse is already full then can't insert anything.
    	 if(Valuable.getValue()<1 || isFull())
         {
             return false;
         }
    	money.add(Valuable);
        return true;
    }
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of money withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Valuable[] withdraw( double amount ) {
        //if ( ??? ) return ???;
        if(amount <= 0)
        {
        	return null;
        }
	   /*
		* One solution is to start from the most valuable Valuable
		* in the purse and take any Valuable that maybe used for
		* withdraw.
		* Since you don't know if withdraw is going to succeed, 
		* don't actually withdraw the money from the purse yet.
		* Instead, create a temporary list.
		* Each time you see a Valuable that you want to withdraw,
		* add it to the temporary list and deduct the value
		* from amount. (This is called a "Greedy Algorithm".)
		* Or, if you don't like changing the amount parameter,
		* use a local total to keep track of amount withdrawn so far.
		* 
		* If amount is reduced to zero (or tempTotal == amount), 
		* then you are done.
		* Now you can withdraw the money from the purse.
		* NOTE: Don't use list.removeAll(templist) for this
		* because removeAll removes *all* money from list that
		* are equal (using Valuable.equals) to something in templist.
		* Instead, use a loop over templist
		* and remove money one-by-one.		
		*/
		if ( amount > 0 && amount <= this.getBalance())
		{
			
			Collections.sort(money , comparator);
			ArrayList<Valuable> out_Valuable = new ArrayList<Valuable>();
			ArrayList<Valuable> clone = new ArrayList<Valuable>();
			for(int run = 0;run<money.size();run++)
			{
				clone.add(money.get(run));
			}
			for(int count = money.size()-1;count>=0;count--)
			{
				if(money.get(count).getValue()<=amount)
				{
					amount -= money.get(count).getValue();
					out_Valuable.add(money.get(count));
					money.remove(count);
				}
			}
			if(0 != amount)
			{
				money = new ArrayList<Valuable>();
				for(int run = 0;run<clone.size();run++)
				{
					money.add(clone.get(run));
				}
				return null;
			}
			Valuable[] arr_out_Valuable = new Valuable[out_Valuable.size()]; 
			out_Valuable.toArray(arr_out_Valuable);
			return arr_out_Valuable;
		}
        return null;
	}
  
    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {
    	return this.count()+" money with value "+this.getBalance();
    }

}